/**
 * MODE: fdfmode_classic
 * =====================
 * Defollow all your following (not defollow users in whitelist) | 90 defollow/hour.
 *
*
 *
 */
class Fdfmode_classic {
    constructor(bot, config, utils) {
        this.bot = bot;
        this.config = config;
        this.utils = utils;
    }
}

module.exports = (bot, config, utils) => { return new Fdfmode_classic(bot, config, utils); };