/**
 * Utils
 * =====================
 * Logger and other functions...
 *
 *
 *              0.2 new pattern
 *              0.3 new sleep system
 *
 */
class Utils {
    constructor(bot, config) {
        this.bot = bot;
        this.config = config;
    }

    /**
     * Logger
     * =====================
     * Better than console.log() 

     */
    logger(type, func, text) {
        console.log(type + " " + func + ": " + text);
    }

    /**
     * Screenshot
     * =====================
     * Save screenshot from chrome

     */
    async screenshot(func, name) {
        try{
            await this.bot.screenshot({ path: './logs/screenshot/' + this.config.instagram_username + '_' + func + '_' + name + '.jpg' });
        } catch (err) {
            this.logger("[WARNING]", "screenshot", "error "+ err);
        }
        
    }

    /**
     * Random
     * =====================
     * Random number between two numbers

     */
    random_interval(min, max) {
        return (Math.floor(Math.random() * (max - min + 1)) + min) * 1000;
    }

    /**
     * Sleep
     * =====================
     * Zzz

     */
    sleep(sec) {
        let sleep = require('system-sleep');
        sleep(sec);
    }

}

module.exports = (bot, config, utils) => { return new Utils(bot, config, utils); };