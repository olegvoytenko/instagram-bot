module.exports = {
    "debug": true,

    // BOT Configs
    // [WORK] likemode_classic - select random hashtag from config list and like 1 random photo (of last 20) | 400-600 like/day.
    // [WORK] likemode_realistic - select random hashtag from config list, like fast 10-12 photo and sleep 15-20min. Sleep at night 
    // [TODO] likemode_superlike - select random hashtag from config list and like 3 random photo of same user | 400-600 like/day.
    // [TODO] fdfmode_classic - follow user from random hashtag and defollow after 1h | 300 follow-defollow/day.
    // [TODO] fdfmode_defollowall - defollow all your following (not defollow users in whitelist) | 90 defollow/hour.
    "bot_mode" : "likemode_classic",
    "bot_likeday_min": 400,
    "bot_likeday_max": 600, //in 2018 limit instagram is descreased to 400-600/day :(
    "bot_sleep_night": "7:00", //sleep from 00:00 to 7:00 am, work only in likemode_realistic
    "bot_fastlike_min": "15", //min minutes of sleep after like 10-12 photo, work only in likemode_realistic
    "bot_fastlike_max": "20", //max minutes of sleep after like 10-12 photo, work only in likemode_realistic

    // Instagram Account
    "instagram_username": "dex_society", //without @
    "instagram_password": "123321123321",
    "instagram_hashtag": ['instagood','like4like','followme', 'smile', 'likeforlike', 'vscocam'], //without #
    "instagram_pin": "email", //method to receive pin (email or sms)
    "instagram_userwhitelist": [''], //without @

    // Puppeteer configs
    "chrome_headless" : true,
    "chrome_options": ['--disable-gpu', '--no-sandbox', '--window-size=1920x1080'],
};
